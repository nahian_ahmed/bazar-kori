"""BazarKori URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include,path
from django.views.generic.base import TemplateView
from accounts.views import HomeView
from rest_framework import routers

from django.contrib import admin
from django.contrib.auth import views as auth_views
from products.views import *
from products import views
from django.conf import settings
from django.conf.urls.static import static


#APi router and paths
router = routers.DefaultRouter()
#check API endpoint
router.register('api/products', views.ItemViewSet)

admin.autodiscover()



urlpatterns = [

	path('home/',HomeView.as_view(),name='home'),

    path('login/', auth_views.login, name='login'),
    path('logout/', auth_views.logout, name='logout'),
    path('oauth/', include('social_django.urls', namespace='social')),

    path('api-auth/', include('rest_framework.urls',namespace='rest_framework')),
    path('', include(router.urls)),

	path('admin/', admin.site.urls),
    path('accounts/',include('accounts.urls')),

    path('cart/', include('cart.urls')),
    path('order/', include('order.urls')),
	path('products/', include('products.urls')),
    path('search/',include('search.urls')),
    path('accounts/',include('django.contrib.auth.urls')),
   



]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
