from django.urls import path
from . import views
from accounts.views import HomeView

app_name = 'accounts'

urlpatterns=[
	# /accounts/

	path('signup/',views.signup,name='signup'),
	
	#This url in main settings
	#path('',HomeView.as_view(),name='home'),


]
