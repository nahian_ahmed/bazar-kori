#from django.shortcuts import render,redirect
#from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from django.contrib.auth import login, authenticate
from django.urls import reverse_lazy
from products.models import Inventory,Item
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from accounts.forms import SignUpForm
# Create your views here.

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


class HomeView(TemplateView):
	print("called")
	template_name = 'home.html'

	def get(self,request):
		print("get home")
		#Check If anonymous user
		if(request.user.is_authenticated ):
			user_name = request.user.get_full_name()
			context = {
				"user_name":user_name
			}
		else :
			user_name = "Guest"
			context = {
					"user_name":user_name
				}
		return render(request,self.template_name,context) 



	def get_featured_data(self,request):
		
		print("featured funstion")
		context = Item.objects.filter(title__contains="Ban")
		user_name = request.user.get_full_name()
		context = {
			"featured_products":featured_products,
			"user_name":user_name
		}

		return render(request,self.template_name,context) 


