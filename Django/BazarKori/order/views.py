from django.shortcuts import render
from .models import OrderItem
from .forms import OrderCreateForm
from cart.cart import Cart
from products.models import Item
#from .tasks import order_created
# Create your views here.


def order_create(request):
	cart = Cart(request)
	print("Order Create function")
	if request.method == 'POST':
		form =OrderCreateForm(request.POST)
		print("postIf")
		if form.is_valid():
			order = form.save()
			for item in cart:
				#save item in order item table
				OrderItem.objects.create(order=order,
										product=item['product'],
										price=item['price'],
										quantity=item['quantity'])

				#Decrement The Product Quantity From Database
				product_name = item['product']
				product = Item.objects.get(title=product_name)
				product.quantity = product.quantity - int(item['quantity'])
				product.save()
				
				#clear cart

				cart.clear()

				

				return render(request, 'order/created.html',{'order':order})

	else:
		print("Form Else")
		form = OrderCreateForm()

	return render(request, 'order/create.html',{'cart':cart,'form':form})

