from django.urls import path
from django.views.generic import TemplateView
from search.views import SearchView,SpeechView

from . import views

urlpatterns = [
	#/searchPage/
    #path('/search_result', views.search_result, name='search_result'),
    path('', SearchView.as_view(), name='search_result'),
    path('speech',SpeechView.as_view(), name='speech_result'),
    path('atsearch',views.auto_complete_search, name='auto_complete_search'),

]
