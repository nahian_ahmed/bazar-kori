from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from products.models import Item
from django.db import models
from django.views.generic import TemplateView

import json
from django.core import serializers

from django.http import HttpResponseRedirect

from .forms import SearchForm

#import the logging library
import logging


#Create Instance of logger
logger = logging.getLogger(__name__)


# Create your views here.



class SearchView(TemplateView):
        #Search Template View class

        template_name= 'search.html'

        #GET method for page
        def get(self,request):

                form = SearchForm()
                return render(request, self.template_name, {'form': form})

        
        #POST the value input to search
        def post(self,request):

                form = SearchForm(request.POST)

                if form.is_valid():

                        search_text  = form.cleaned_data['search_input'] 

                        #Split Defintions change only  here
                        search_input = search_text.split('.')
                        len_search_items = len(search_input)

                        #Create search query for appropritated searched products

                        if(len_search_items==1):
                                search_split = search_input[0]
                                #Store the split search text in Array

                                searched_products=list(Item.objects.filter(title__contains=search_split))
                                args = { 'len_search_items':len_search_items,'search_split':search_split,'form':form, 'search_input':search_input,'searched_products':searched_products,'search_text':search_text}

                        elif(len_search_items==2):
                                search_split = search_input[0]
                                #Store the split search text in Array

                                searched_products=list(Item.objects.filter(title__contains=search_split))
                                
                                search_split1 = search_input[1]
                                #Store the split search text in Array

                                searched_products1=list(Item.objects.filter(title__contains=search_split1))
                                args = { 'search_split1':search_split1,'searched_products1':searched_products1,
                                        'len_search_items':len_search_items,'search_split':search_split,'form':form, 'search_input':search_input,'searched_products':searched_products,'search_text':search_text}

                        elif(len_search_items==3):
                                search_split = search_input[0]
                                #Store the split search text in Array

                                searched_products=list(Item.objects.filter(title__contains=search_split))
                                
                                search_split1 = search_input[1]
                                #Store the split search text in Array

                                searched_products1=list(Item.objects.filter(title__contains=search_split1))
                                
                                search_split2 = search_input[2]
                                #Store the split search text in Array

                                searched_products2=list(Item.objects.filter(title__contains=search_split2))

                                args = { 'search_split1':search_split1,'searched_products1':searched_products1,
                                        'search_split2':search_split2,'searched_products2':searched_products2,
                                        'len_search_items':len_search_items,'search_split':search_split,'form':form, 'search_input':search_input,'searched_products':searched_products,'search_text':search_text}
                        elif(len_search_items==4):
                                search_split = search_input[0]
                                #Store the split search text in Array
                                searched_products=list(Item.objects.filter(title__contains=search_split))
                                
                                search_split1 = search_input[1]
                                #Store the split search text in Array
                                searched_products1=list(Item.objects.filter(title__contains=search_split1))
                                
                                search_split2 = search_input[2]
                                #Store the split search text in Array
                                searched_products2=list(Item.objects.filter(title__contains=search_split2))
                                
                                search_split3 = search_input[3]
                                #Store the split search text in Array
                                searched_products3=list(Item.objects.filter(title__contains=search_split3))
                                
                                args = { 'search_split1':search_split1,'searched_products1':searched_products1,
                                        'search_split2':search_split2,'searched_products2':searched_products2,
                                        'search_split3':search_split3,'searched_products3':searched_products3,
                                        'len_search_items':len_search_items,'search_split':search_split,'form':form, 'search_input':search_input,'searched_products':searched_products,'search_text':search_text}

                        elif(len_search_items==5):
                                search_split = search_input[0]
                                #Store the split search text in Array
                                searched_products=list(Item.objects.filter(title__contains=search_split))
                                
                                search_split1 = search_input[1]
                                #Store the split search text in Array
                                searched_products1=list(Item.objects.filter(title__contains=search_split1))
                                
                                search_split2 = search_input[2]
                                #Store the split search text in Array
                                searched_products2=list(Item.objects.filter(title__contains=search_split2))
                                
                                search_split3 = search_input[3]
                                #Store the split search text in Array
                                searched_products3=list(Item.objects.filter(title__contains=search_split3))
                                
                                search_split4 = search_input[4]
                                #Store the split search text in Array
                                searched_products4=list(Item.objects.filter(title__contains=search_split4))
                                
                                args = { 'search_split1':search_split1,'searched_products1':searched_products1,
                                        'search_split2':search_split2,'searched_products2':searched_products2,
                                        'search_split3':search_split3,'searched_products3':searched_products3,
                                        'search_split4':search_split4,'searched_products4':searched_products4,
                                        'len_search_items':len_search_items,'search_split':search_split,'form':form, 'search_input':search_input,'searched_products':searched_products,'search_text':search_text}

                        else:
                                return HttpResponse("Maximum 5 product Multi Search")

                        

                
                        return render(request,self.template_name,args) 
                else:
                        return render(request,'search.html')
        



        def searched_item(search_text):

                        search_input = search_text.split()
                        search_split = search_input[0]  
                        #Query for search result. Search Filter function can be changed here
                        searched_products=Item.objects.filter(title__contains=search_split)




class SpeechView(TemplateView):
        #Search Template View class
        template_name= 'SpeechSearch.html'
        #POST the value input to search
        def post(self,request):

                speech_input=request.POST['q']

                searched_products=list(Item.objects.filter(title__contains=speech_input))
                                
                context={
                        'speech_input' :speech_input,
                        'searched_products' :searched_products,
                }
                if not searched_products:
                    # Log an error message
                    logger.error('No voice searched products found in Database!')

                return render(request,'SpeechSearch.html',context)



def auto_complete_search(request):
       
        data = Item.objects.values_list('title')
        data_list = list(data)

        #List for storing converted products title string
        list1=[]
        for item in data: 
            i_str=str(item)
            i_str=i_str.strip("() ,'")
            list1.append(i_str)

        if request.method == 'POST':
            speech_input = request.POST['t']
            print(speech_input)
            searched_products=list(Item.objects.filter(title__contains=speech_input))
                                
            context={
                        'speech_input' :speech_input,
                        'searched_products' :searched_products,
            }
            return render(request,'SpeechSearch.html',context)

        print(type(data))
        return render(request,'aut_cmplt_search.html',{'data':list1})