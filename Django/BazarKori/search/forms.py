from django import forms

class SearchForm(forms.Form):
    search_input = forms.CharField(label='Search', max_length=100,widget= forms.TextInput
                           (attrs={
                           			'class': 'input search-input',
                           			'placeholder':'e.g: Chaal . Daal '
                           	}))

