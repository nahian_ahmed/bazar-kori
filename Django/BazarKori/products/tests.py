from django.test import TestCase
import unittest
from products.models import Item,Inventory
from products.views import products
from django.urls import resolve


# Create your tests here.
class HomePageTest(TestCase):

	def test_root_url_resolve(self):
		found = resolve('/products')
		self.assertEqual(found.func, products)