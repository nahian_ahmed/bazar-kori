from django.urls import path

from . import views

urlpatterns = [
	#/products/
    path('', views.products, name='products'),

    #/products/<product_id>/
    path('<int:item_id>/',views.detail,name='detail'),

    #/products/category/<category_id>/
    path('category/<int:item_id>/',views.category_product,name='category_product'),

]
