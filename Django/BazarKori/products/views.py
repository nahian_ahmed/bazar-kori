from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Item, Inventory
from cart.forms import CartAddProductForm
from rest_framework import viewsets
from rest_framework import permissions
from products.serializers import ItemSerializer


# Create your views here.

#All products in the inventory
def products(request):
	all_category = Inventory.objects.all()

	return render(request, 'products.html',{'all_category': all_category,})
#detiald of individual products
def detail(request,item_id):

	item= get_object_or_404(Item, pk=item_id)

	cart_product_form = CartAddProductForm()


	return render(request , 'details.html',{'item':item,'cart_product_form':cart_product_form})


#Products According to category
def category_product(request,item_id):
	#item= get_object_or_404(Inventory, pk=item_id)
	category= Inventory.objects.get(pk=item_id)
	#print(category.name)
	category_name=category.name
	all_products = Item.objects.filter(category=item_id)

	return render(request , 'category_products.html',{'all_products':all_products,'category_name':category_name})

#API Serializer view class

class ItemViewSet(viewsets.ModelViewSet):
	#fetch all rows of data from Item table
	queryset = Item.objects.all()
	serializer_class = ItemSerializer