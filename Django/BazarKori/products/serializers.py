from .models import Item,Inventory
from rest_framework import serializers

class ItemSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Item
		fields = ('title','price','featured','img','quantity')