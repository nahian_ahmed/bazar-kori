from decimal import Decimal
from django.conf import settings
from products.models import Item

class Cart(object):

	def __init__(self, request):
		"""Initialize a Cart """
		self.session = request.session

		cart = self.session.get(settings.CART_SESSION_ID)

		if not cart:
			#save empty cart in session
			cart = self.session[settings.CART_SESSION_ID] = {}

		self.cart = cart

	def add(self , product, quantity=1, update_quantity=False):
		"""
		Add Product to Cart or Update Quantity
		"""

		product_id = str(product.id)

		if product_id not in self.cart:

			self.cart[product_id] = {'quantity':0,
									  'price': str(product.price)
									}

		if update_quantity:

			self.cart[product_id]['quantity'] = quantity

		else:
			self.cart[product_id]['quantity'] += quantity

		self.save()

	def save(self):
		"""Update the session cart"""
		self.session[settings.CART_SESSION_ID] = self.cart

		#Mark session as modified to make sure it is saved

		self.session.modified = True

	def remove(self, product):

		"""Remove a product from cart """
		product_id = str(product.id)

		if product_id in self.cart:
			del self.cart[product_id]
			self.save()

	def __iter__(self):
		"""Get the product objects from database and add them to cart"""

		product_ids = self.cart.keys()
		
		#Filter product objects and add  them to cart

		products = Item.objects.filter(id__in=product_ids)

		for product in products:
			self.cart[str(product.id)]['product'] = product

		for item in self.cart.values():
			item['price'] = Decimal(item['price'])
			item['total_price'] = item['price'] * item['quantity']
			yield item


	def __len__(self):
		"""Count All Items In cart"""

		return sum(item['quantity'] for item in self.cart.values())

	def get_total_price(self):

		return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

	def clear(self):
		#clear cart

		self.session[settings.CART_SESSION_ID] = {}
		self.session.modified = True