from django.shortcuts import render,redirect, get_object_or_404
from django.views.decorators.http import require_POST

from django.db.models import Sum
from .cart import Cart
from products.models import Item
from .forms import CartAddProductForm



# Create your views here.
"""
def cart_detail(request):
    curt_user = request.user.id

    cart_products = cart_table.objects.all().filter(pk=curt_user)
    

    return render(request, 'cart/cart_detail.html', {'cart_products' : cart_products, 'curt_user' : curt_user})
    """

@require_POST
def cart_add(request, item_id):
	cart = Cart(request)

	product = get_object_or_404(Item, id=item_id)

	form = CartAddProductForm(request.POST)

	if form.is_valid():

		cd = form.cleaned_data

		#Cart quantity auto synchronize according to stock
		product_stock = product.quantity
		quantity 	  = cd['quantity']
		if product_stock < quantity:
			quantity = product_stock

		cart.add(product=product,
				quantity=quantity,
				update_quantity=cd['update'])



	return redirect('cart_detail')


def cart_remove(request, product_id):

	cart = Cart(request)

	product = get_object_or_404(Item, id=product_id)

	cart.remove(product)

	return redirect('cart_detail')

def cart_detail(request):

	cart = Cart(request)
	# Check if any user logged in
	if(request.user.is_authenticated ):
		user = request.user.get_full_name()
	else:
		user = "Guest"

	return render(request, 'cart/detail.html', {'cart':cart,'user':user})


