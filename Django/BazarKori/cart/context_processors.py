from .cart import Cart

#cart context processors
def cart(request):
	return {'cart':Cart(request) }