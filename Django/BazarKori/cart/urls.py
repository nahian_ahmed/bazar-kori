from django.urls import path
from . import views

urlpatterns = [
	#/cart
    path('', views.cart_detail, name='cart_detail'),
    path('add/<int:item_id>/', views.cart_add, name='cart_add'),
    path('remove/<int:product_id>/', views.cart_remove, name='cart_remove'),

     #/products/category/<category_id>/
    #path('category/<int:item_id>/',views.category_product,name='category_product'),
]